package in.piksal.shadowtalk;

import android.content.Context;
import android.content.Intent;

/**
 * Created by Thien on 7/23/2015.
 */
public class QRCodeResult {
    private String raw; //raw result from qr code
    public Boolean isValid;


    public QRCodeResult(String raw) {
        this.raw = raw;
        verify();
    }

    public String getRaw() {
        return raw;
    }

    public String getImageName(){
        return raw+".png";
    }

    public String getSoundName(){
        return raw+".mp3";
    }

    private void verify(){
        //TODO: check if it available to display

        isValid = true;
    }

    public static void gotoCameraActivity(Context context){
        Intent intent = new Intent(context,CameraActivity.class);
        context.startActivity(intent);
    }

}
