package in.piksal.shadowtalk.CustomListView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import in.piksal.shadowtalk.R;

/**
 * Created by Thien on 8/12/2015.
 */
public class ImageTextViewGroup extends RelativeLayout {

    public TextView TextContent;

    private ImageView Lock;
    private ImageView ImageView2;

    public ImageTextViewGroup(Context context) {
        super(context);

        //use LayoutInflater
        LayoutInflater li = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        li.inflate(R.layout.image_text_item, this, true);//bind CustomViewGroup with List_item layout

        //bind
        TextContent = (TextView) findViewById(R.id.textView);
        ImageView2 = (ImageView) findViewById(R.id.imageView_id);
        Lock = (ImageView) findViewById(R.id.lockImage);
        Lock.setVisibility(INVISIBLE);

    }

    public void setLock(boolean o){
        if (o == true)
            Lock.setVisibility(VISIBLE);
        else Lock.setVisibility(INVISIBLE);
    }
    public void setImage(Bitmap bitmap){
        ImageView2.setImageBitmap(bitmap);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(),bitmap);

    }

    public void setImage(Drawable drawable){
        ImageView2.setImageDrawable(drawable);

    }

    public void setText(String text) {
        TextContent.setText(text);
    }
}
