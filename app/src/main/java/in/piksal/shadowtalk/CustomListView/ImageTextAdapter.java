package in.piksal.shadowtalk.CustomListView;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

/**
 * Created by Thien on 8/19/2015.
 */
public class ImageTextAdapter extends ArrayAdapter<ImageText> {
    private Context context;
    private int resource;
    private ArrayList<ImageText> objects;

    //constructor
    public ImageTextAdapter(Context context,@LayoutRes int resource, ArrayList<ImageText> objects) {
        super(context, resource, objects);
        this.context=context;
        this.resource=resource;
        this.objects=objects;
    }


    @Override //getView
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageTextViewGroup Item_view = (ImageTextViewGroup)convertView;
        if (Item_view==null)
            Item_view = new ImageTextViewGroup(getContext());

        //the data, which contain Title and Description
        ImageText myData = objects.get(position);

        String s1 = myData.text.substring(0,1).toUpperCase();
        String s2 = myData.text.substring(1);
        String text = s1+s2;

        Item_view.setImage(myData.bitmap);
        Item_view.setText(text);
        Item_view.setLock(myData.isLock);


        return Item_view;//return
    }
}
