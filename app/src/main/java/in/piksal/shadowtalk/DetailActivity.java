package in.piksal.shadowtalk;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.InputStream;

public class DetailActivity extends AppCompatActivity {

    private String LOG_TAG = DetailActivity.class.getSimpleName();

    private String ScanResult = null;
    private QRCodeResult qrCodeResult = null;
    private ImageView imageView;


    //player
    String currentSoundName;
    MediaPlayer player = null;
    Boolean isPlaying = false;

    //button
    ImageView qrButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        imageView = (ImageView) findViewById(R.id.detailImageView_id);

        getResultFromIntent();
        qrCodeResult=new QRCodeResult(ScanResult);
        displayImage_playSound(qrCodeResult);

        qrButton = (ImageView) findViewById(R.id.qrButton_id);
        qrButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPlayer();
                QRCodeResult.gotoCameraActivity(DetailActivity.this);
            }
        });
    }



    private void getResultFromIntent(){
        Bundle data = getIntent().getExtras();
        ScanResult = data.getString(getResources().getString(R.string.extra_qrresult));
        setTitle(ScanResult);

    }

    private void displayImage_playSound(QRCodeResult qrCodeResult){
        //new ProcessImage().execute(qrCodeResult);
        InputStream image;
        String imageName = qrCodeResult.getImageName();
        String soundName = qrCodeResult.getSoundName();

        try {
            image = getAssets().open(imageName);
            Drawable drawable = Drawable.createFromStream(image,null);
            imageView.setImageDrawable(drawable);
            //show bitmap

        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e(LOG_TAG, "DisplayImage error");
        }
        //play sound


       // turnOnFlashOnly();//turn on flash only from camera (not flash of QRScanner)
        playSound(soundName);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPlaying)
                    stopPlayer();
                playSound(currentSoundName);
            }
        });
    };

    private void playSound(String filename ){
        try {
            AssetFileDescriptor afd = getAssets().openFd(filename);
            currentSoundName = filename;
            player = new MediaPlayer();
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            player.prepare();
            player.start();
            isPlaying = true;
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    //called when finish meowwwww
                    showTouchMe();
                    Log.i(LOG_TAG, "player on complete");
                    freePlayer();
                }
            });
        }catch (Exception e){e.printStackTrace();Log.e(LOG_TAG,"play sound error");};
    }

    private void stopPlayer(){
        player.stop();
        freePlayer();
    }

    //show pop up "Touch me"
    private void showTouchMe(){
        Toast touchme = Toast.makeText(this,"Touch me",Toast.LENGTH_LONG);
        touchme.setGravity(Gravity.CENTER,0,0);
        touchme.show();
    }

    private void freePlayer(){
        isPlaying = false;
        player.release();//release player
        player = null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)&&isPlaying) {
            //Stop meow

            stopPlayer();
            return true;

        }

        return super.onKeyDown(keyCode, event);
    }

}
