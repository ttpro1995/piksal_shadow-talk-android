package in.piksal.shadowtalk;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import in.piksal.shadowtalk.CustomListView.ImageTextViewGroup;

public class ShopItemPreviewActivity extends AppCompatActivity {
    private ImageTextViewGroup pic1=null;
    private ImageTextViewGroup pic2=null;
    private ImageTextViewGroup pic3=null;
    private ImageTextViewGroup pic4=null;
    private ImageTextViewGroup buyNow = null;


    private FrameLayout container_pic1=null;
    private FrameLayout container_pic2=null;
    private FrameLayout container_pic3=null;
    private FrameLayout container_pic4=null;
    private FrameLayout container_buyNow=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_item_preview);
        show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_shop_item_preview, menu);
        return true;
    }

    private void show(){
        container_pic1 = (FrameLayout) findViewById(R.id.pic1);
        container_pic2 = (FrameLayout) findViewById(R.id.pic2);
        container_pic3 = (FrameLayout) findViewById(R.id.pic3);
        container_pic4 = (FrameLayout) findViewById(R.id.pic4);
        container_buyNow = (FrameLayout) findViewById(R.id.buyNow);



        pic1 = new ImageTextViewGroup(ShopItemPreviewActivity.this);
        pic2 = new ImageTextViewGroup(ShopItemPreviewActivity.this);
        pic3 = new ImageTextViewGroup(ShopItemPreviewActivity.this);
        pic4 = new ImageTextViewGroup(ShopItemPreviewActivity.this);
        buyNow = new ImageTextViewGroup(ShopItemPreviewActivity.this);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
