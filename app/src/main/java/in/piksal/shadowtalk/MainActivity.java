package in.piksal.shadowtalk;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import in.piksal.shadowtalk.CustomListView.ImageText;
import in.piksal.shadowtalk.CustomListView.ImageTextAdapter;
import in.piksal.shadowtalk.CustomListView.ImageTextViewGroup;
import in.piksal.shadowtalk.Data.Album;

public class MainActivity extends AppCompatActivity  {

    private String LOG_TAG = MainActivity.class.getSimpleName();

    private ImageTextViewGroup pic1=null;
    private ImageTextViewGroup pic2=null;
    private ImageTextViewGroup pic3=null;
    private ImageTextViewGroup pic4=null;
    private ImageTextViewGroup showShadowButton=null;

    private FrameLayout container_pic1=null;
    private FrameLayout container_pic2=null;
    private FrameLayout container_pic3=null;
    private FrameLayout container_pic4=null;
    private FrameLayout container_showShadow=null;

    private ListView listView;
    private ImageView qrButton;
    ArrayList<ImageText> datas;

    private ArrayList<Album> album_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        setTitle("Shadow Talk");


        listView = (ListView)findViewById(R.id.listView_id);
        show();
        qrButton = (ImageView) findViewById(R.id.qrButton_id);
        qrButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoCameraActivity();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                chooseAlbum(position);
            }
        });
    }

    private void show(){
        album_list = new ArrayList<>();
        datas = new ArrayList<>();
        try {
            InputStream json_is = getAssets().open("data.json");
            String rawJson = Album.inputstream_to_string(json_is);
            album_list = Album.getAllAlbums(rawJson);


            for (int i = 0; i < album_list.size(); i++) {
                Album tmp = album_list.get(i);
                ImageText data= new ImageText();
                InputStream is = getAssets().open(tmp.getCover());
                Bitmap bitmap = BitmapFactory.decodeStream(is);
                data.bitmap = bitmap;
                data.text = tmp.getName();
                if (i == 0) data.isLock = false; else data.isLock = true;
                datas.add(data);
            }
            ImageTextAdapter adapter = new ImageTextAdapter(this,R.layout.image_text_item,datas);
            listView.setAdapter(adapter);
        }catch (IOException e){e.printStackTrace();}
    }

    private void chooseAlbum(int position){
        ImageText data = datas.get(position);
        if (data.isLock){
            // TODO: 8/19/2015 buy
        }
        else {
            // TODO: 8/19/2015 show album
            Intent intent = new Intent(MainActivity.this,ShowAlbumActivity.class);
            intent.putExtra(getResources().getString(R.string.extra_album_name),album_list.get(position).getName());
            Log.i(LOG_TAG,"put extra name = "+album_list.get(position).getName());
            startActivity(intent);
        }
    }

    private void gotoCameraActivity(){
        Intent intent = new Intent(MainActivity.this,CameraActivity.class);
        startActivity(intent);
    }

    private void shopItemPreview(){
        Intent intent = new Intent(MainActivity.this,ShopItemPreviewActivity.class);
        //put item name here
        startActivity(intent);
    }

    private void showPackDetail(){
        //TODO:
        Intent intent = new Intent(MainActivity.this,PackDetailActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
