package in.piksal.shadowtalk;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileDescriptor;
import java.io.InputStream;

import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;


public class CameraActivity extends AppCompatActivity {
    private String LOG_TAG = CameraActivity.class.getSimpleName();

    ZBarScannerView QrScannerView;// scanner view which we will add into frameLayout
    Boolean FlashMode = true; //flash on or off
    String CAMERA_ACTIVITY_TAG = "CAMERA_ACTIVITY";
    String LIFECYCLE_TAG="LifeCycle";

    ZBarScannerView.ResultHandler QRresultHandler;//result handler for QR

    //view that on layout
    FrameLayout CameraframeLayout;// a placeholder for code scanner preview
    ImageView imageView; // display image



    //player
    private String currentSoundName;

    //button
    private ImageView flash;
    private Button screenOff;
    private Button flashingButton;

    //screen status
    int screen_status = 1;
    int SCREEN_STATUS_OFF = 0;
    int SCREEN_STATUS_ON = 1;
    private GestureDetectorCompat mDetector;
    int ScreenBrightness;

    //flashing
    Boolean isFlashing = false;
    Thread flashing = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        //button
        flash = (ImageView) findViewById(R.id.mFlash_id);
        screenOff = (Button) findViewById(R.id.screenOff_id);
        flashingButton = (Button) findViewById(R.id.flashing_id);


        //find framLayout
        CameraframeLayout = (FrameLayout) findViewById(R.id.mCameraFrame_id);
        imageView = (ImageView) findViewById(R.id.mImageResultView_id);
        //Create QR Scanner View
        QrScannerView = new ZBarScannerView(CameraActivity.this);
        //add QRScannerView to FrameLayout
        CameraframeLayout.addView(QrScannerView);

        //init result handler
        CreateResultHandle();

        flash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                turnFlashOnOff();
            }
        });

        flashingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                turnFlashing();
            }
        });

        screenOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                turnScreenOff();
            }
        });

        createGestureDetector();


    }


    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();
        unlockScreen();
        turnOnQRScanner();

    }

    @Override
    protected void onPause() {
        super.onPause();
        QrScannerView.stopCamera();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void turnFlashing(){
        if (isFlashing)
            isFlashing = false;
        else {
            isFlashing = true;
            flashing = new Thread(){
                @Override
                public void run() {
                    super.run();
                    while (isFlashing){
                        try{
                            Thread.sleep(1000);
                            turnFlashOnOff();
                        }catch (InterruptedException e){e.printStackTrace();}
                    }
                }
            };
            flashing.start();//start flashing thread
        }
    }


    //Create result handler, put this into onCreate
    private void CreateResultHandle(){
        QRresultHandler = new ZBarScannerView.ResultHandler() {
            @Override
            public void handleResult(Result result) {
                Log.v(CAMERA_ACTIVITY_TAG, result.getContents()); // Prints scan results
                Log.v(CAMERA_ACTIVITY_TAG, result.getBarcodeFormat().getName()); // Prints the scan format (qrcode, pdf417 etc.)
                QRCodeResult qrCodeResult = new QRCodeResult(result.getContents());

                gotoDetailActivity(qrCodeResult.getRaw());
                    //displayImage_playSound(qrCodeResult);
                Toast.makeText(CameraActivity.this,qrCodeResult.getImageName(),Toast.LENGTH_SHORT).show();

            }
        };
    }

    private void gotoDetailActivity(String rawResult){
        Intent intent = new Intent(CameraActivity.this,DetailActivity.class);
        intent.putExtra(getResources().getString(R.string.extra_qrresult), rawResult);
        startActivity(intent);
    }



    /*Turn on QRScanner Camera
      */
    private void turnOnQRScanner() {
        //TODO: SET Result Handle Here
        QrScannerView.setResultHandler(QRresultHandler);
        //TODO Start camera
        QrScannerView.startCamera();
        //TODO Set flash and autofocus
        QrScannerView.setFlash(FlashMode);
        QrScannerView.setAutoFocus(true);
    }

     /*
    * */

    /*
    * Change flash mode : On Off
    * */
    private void turnFlashOnOff(){
        if (FlashMode){
            FlashMode = false;
            if (isFlashing)
            flashing.interrupt();
        }else
            FlashMode = true;

        QrScannerView.setFlash(FlashMode);
    }




    private void turnScreenOff(){
        Log.i(LOG_TAG,"turn screen off");

            WindowManager.LayoutParams params = getWindow().getAttributes();
            params.screenBrightness = 0.01f;
            getWindow().setAttributes(params);
            screen_status = SCREEN_STATUS_OFF;

    }

    private void turnScreenOn(){

            Log.i(LOG_TAG, "turn screen on");
            WindowManager.LayoutParams params = getWindow().getAttributes();
            params.screenBrightness = 1;
            getWindow().setAttributes(params);
            screen_status =SCREEN_STATUS_ON;

    }

    private void unlockScreen() {
        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }

    private void createGestureDetector(){

        mDetector = new GestureDetectorCompat(this, new GestureDetector.OnGestureListener() {
            @Override
            public boolean onDown(MotionEvent e) {
                return false;
            }

            @Override
            public void onShowPress(MotionEvent e) {

            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return false;
            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                return false;
            }

            @Override
            public void onLongPress(MotionEvent e) {

            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                return false;
            }
        });
        mDetector.setOnDoubleTapListener(new GestureDetector.OnDoubleTapListener() {
            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                return true;
            }

            @Override
            public boolean onDoubleTap(MotionEvent e) {
                Log.i(LOG_TAG, "DoubleTap");
                turnScreenOn();
                return true;
            }

            @Override
            public boolean onDoubleTapEvent(MotionEvent e) {
                return false;
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mDetector.onTouchEvent(event);

        return super.onTouchEvent(event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)&&isFlashing) {
            //Stop flashing
            turnFlashing();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
