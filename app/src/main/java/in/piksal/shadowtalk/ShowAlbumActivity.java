package in.piksal.shadowtalk;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import in.piksal.shadowtalk.CustomListView.ImageText;
import in.piksal.shadowtalk.CustomListView.ImageTextAdapter;
import in.piksal.shadowtalk.Data.Album;

public class ShowAlbumActivity extends AppCompatActivity {
    private int export_mode;
    private int EXPORT_1_SIDE =0;
    private int EXPORT_2_SIDE = 1;
    private ListView listView;
    private ImageView qrButton;
    private ArrayList<ImageText> datas;
    private String albumName;
    private ArrayList<Album> album_list;
    private Album currentAlbum;
    private ArrayList<String> albumContent;
    private Button exportButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_album);
        getFromIntent();
        listView = (ListView)findViewById(R.id.listView_id);
        qrButton = (ImageView) findViewById(R.id.qrButton_id);
        show();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showDetail(position);
            }
        });
        qrButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QRCodeResult.gotoCameraActivity(ShowAlbumActivity.this);
            }
        });
        exportButton = (Button) findViewById(R.id.exportButton_id);
        exportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showExportDialog();
            }
        });
    }

    private void getFromIntent(){
        Bundle data = getIntent().getExtras();
        albumName = data.getString(getResources().getString(R.string.extra_album_name));
        try {
            InputStream json_is = getAssets().open("data.json");
            String rawJson = Album.inputstream_to_string(json_is);
            album_list = Album.getAllAlbums(rawJson);
            currentAlbum = Album.findAlbum(album_list, albumName);
            albumContent = currentAlbum.getAlbum_content();
        }catch (IOException e){e.printStackTrace();}
    }

    private void showExportDialog(){
        final CharSequence[] items = {" 1 side "," 2 sides "};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = ShowAlbumActivity.this.getLayoutInflater();
        builder.setSingleChoiceItems(items, 1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                export_mode = which;
            }
        });
        builder.setTitle(getResources().getString(R.string.export_dialog_title));
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
              if (export_mode == EXPORT_1_SIDE){
                  Log.i("export_dialog","export 1 side");
              };
                if (export_mode == EXPORT_2_SIDE){
                    Log.i("export_dialog","export 2 side");
                };
            }
        });
        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.create().show();
    }

    private void show(){
        try {

        datas = new ArrayList<>();

            for (int i = 0; i < albumContent.size(); i++) {
                ImageText data= new ImageText();
                InputStream is = getAssets().open(albumContent.get(i)+".png");
                Bitmap bitmap = BitmapFactory.decodeStream(is);
                data.bitmap = bitmap;
                data.text = albumContent.get(i);
               data.isLock= false;
                datas.add(data);
            }
            ImageTextAdapter adapter = new ImageTextAdapter(this,R.layout.image_text_item,datas);
            listView.setAdapter(adapter);
        }catch (IOException e){e.printStackTrace();}
    }

    private void showDetail(int position){
        String s= albumContent.get(position);
        Intent intent = new Intent(ShowAlbumActivity.this,DetailActivity.class);
        intent.putExtra(getResources().getString(R.string.extra_qrresult),s);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_show_album, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
