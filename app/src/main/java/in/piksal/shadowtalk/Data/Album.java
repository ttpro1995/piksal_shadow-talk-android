package in.piksal.shadowtalk.Data;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by Thien on 8/20/2015.
 */
public class Album {
    private static String LOG_TAG = Album.class.getSimpleName();
    String name;
    String cover;
    ArrayList<String> album_content;

    public Album(JSONObject jsonObject) {
        getAlbumInfo(jsonObject);
    }

    public String getCover() {
        return cover;
    }

    public String getName() {
        return name;
    }

    public ArrayList<String> getAlbum_content() {
        return album_content;
    }

    public static ArrayList<Album> getAllAlbums(String rawJson){
        ArrayList<Album> result = new ArrayList<>();
        try {
            JSONObject raw = new JSONObject(rawJson);
            JSONArray album = raw.getJSONArray("album");
            for (int i = 0;i<album.length();i++){
                JSONObject cur_album = album.getJSONObject(i);
                Album tmp = new Album(cur_album);
                result.add(tmp);
            }

        }catch (org.json.JSONException e){e.printStackTrace();}
        return result;
    };

    private void getAlbumInfo(JSONObject jsonObject){
        album_content = new ArrayList<>();
        try {
            name = jsonObject.getString("name");
            cover = jsonObject.getString("cover");
            JSONArray jsonArray = jsonObject.getJSONArray("album_content");
            for (int i = 0; i<jsonArray.length();i++){
                String tmp = jsonArray.getString(i);
                album_content.add(tmp);
            }
        }
        catch (JSONException e){e.printStackTrace();}
    }

    public static String inputstream_to_string(InputStream is){
        String result=null;
        try {
            StringBuffer buffer = new StringBuffer();//string here
            BufferedReader reader;

            reader = new BufferedReader(new InputStreamReader(is));//reader will read from inputStream

            String tmp; //temp string

            while ((tmp = reader.readLine()) != null)//when reader is succesfully read and put value into tmp
            {
                buffer.append(tmp);//append tmp into StringBuffer
            }
            result = buffer.toString();// json is here
        }
        catch (Exception e){e.printStackTrace();}
        return result;
    }

    public static Album findAlbum(ArrayList<Album> arrayList,String name){
        for (int i = 0;i<arrayList.size();i++){
            Album tmp = arrayList.get(i);
            Log.i(LOG_TAG,"album name = "+tmp.getName());
            if (tmp.getName().equals(name))
                return tmp;
        }
        return  null;
    }

}
